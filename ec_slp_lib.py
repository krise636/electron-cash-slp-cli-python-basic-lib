import json
import subprocess

# Check if daemon is connected
def check_daemon(electron_cash_path):
    """Check daemon running
    Checks if Electron Cash daemon is running
    """
    daemon_status = subprocess.run(
        [electron_cash_path,"daemon", "status"],
        capture_output=True, text=True
    )
    json_output = json.loads(daemon_status.stdout)
    connected = json_output["connected"]
    
    return connected

# Doesn't support multiple loaded wallet
def check_wallet_loaded(electron_cash_path, wallet_path):
    """Check wallet loaded
    Checks if the wallet is loaded in Electron Cash daemon
    """
    daemon_status = subprocess.run(
        [electron_cash_path,"daemon", "status"],
        capture_output=True, text=True
    )
    json_output = json.loads(daemon_status.stdout)
    wallet_output = json_output["wallets"]
    wallet_loaded = list(wallet_output)[0]
    if wallet_loaded == wallet_path:
        return True
    else:
        return False

def get_unused_bch_address(electron_cash_path, wallet_path):
    """Get unused BCH address
    """
    unused_bch_address = subprocess.run(
        [electron_cash_path,"-w", wallet_path, "getunusedaddress"],
        capture_output=True, text=True
    )
    return unused_bch_address

def get_unused_slp_address(electron_cash_path, wallet_path):
    """Get unused SLP address
    """
    receive_address = subprocess.run(
        [electron_cash_path,"-w", wallet_path, "getunusedaddress_slp"],
        capture_output=True, text=True
    )
    return unused_slp_address

def get_address_balance_bch(electron_cash_path, wallet_path, address):
    """Get the balance of a BCH address
    """
    bch_address_balance = subprocess.run(
        [electron_cash_path, "-w", wallet_path, "getaddressbalance",
        address], capture_output=True, text=True
    )
    bch_address_balance_final = json.loads((bch_address_balance.stdout).strip())
    return bch_address_balance_final

def prepare_transaction(electron_cash_path, wallet_path, tokenIdHex, address, token_amount):
    """Prepare transaction
    Creates the raw transaction data
    """
    tx_data = subprocess.run(
            [electron_cash_path, "-w", wallet_path, "payto",
            address, bch_amount],
            capture_output=True, text=True
        )
    tx_data_json = json.loads(tx_data.stdout)
    tx_hex = tx_data_json['hex']
    return tx_hex

def prepare_slp_transaction(electron_cash_path, wallet_path, tokenIdHex, address, token_amount):
    """Prepare SLP transaction
    Creates the raw SLP transaction data
    """
    tx_data = subprocess.run(
            [electron_cash_path, "-w", wallet_path, "payto_slp",
            tokenIdHex, address, token_amount],
            capture_output=True, text=True
        )
    tx_data_json = json.loads(tx_data.stdout)
    tx_hex = tx_data_json['hex']
    return tx_hex

def broadcast_tx(electron_cash_path, wallet_path, tx_hex):
    """Broadcast transaction
    Send the transaction to the network
    """
    broadcast = subprocess.run(
            [electron_cash_path, "-w", wallet_path, "broadcast", tx_hex], capture_output=True, text=True
        )
    tx_id_json =  json.loads(broadcast.stdout)
    tx_id = tx_id_json[1]       
    return tx_id
